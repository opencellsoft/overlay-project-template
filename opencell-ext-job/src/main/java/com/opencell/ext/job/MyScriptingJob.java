package com.opencell.ext.job;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.meveo.admin.job.ScriptingJobBean;
import org.meveo.model.crm.CustomFieldTemplate;
import org.meveo.model.crm.EntityReferenceWrapper;
import org.meveo.model.crm.custom.CustomFieldTypeEnum;
import org.meveo.model.jobs.JobCategoryEnum;
import org.meveo.model.jobs.JobExecutionResultImpl;
import org.meveo.model.jobs.JobInstance;
import org.meveo.model.jobs.MeveoJobCategoryEnum;
import org.meveo.model.scripts.ScriptInstance;
import org.meveo.service.job.Job;

/**
 * The Class ScriptingJob execute the given script.
 *
 * @author anasseh
 * @author Abdellatif BARI
 * @lastModifiedVersion 7.0
 */
@Stateless
public class MyScriptingJob extends Job {

    public static final String MY_SCRIPTING_JOB_SCRIPT = "MyScriptingJob_script";

    /**
     * The scripting job bean.
     */
    @Inject
    ScriptingJobBean scriptingJobBean;

    @Override
    protected JobExecutionResultImpl execute(JobExecutionResultImpl result, JobInstance jobInstance) {
        String scriptCode = null;
        try {
            scriptCode = ((EntityReferenceWrapper) this.getParamOrCFValue(jobInstance, MY_SCRIPTING_JOB_SCRIPT)).getCode();
            Map<String, Object> context = (Map<String, Object>) this.getParamOrCFValue(jobInstance, "MyScriptingJob_variables");
            if (context == null) {
                context = new HashMap<>();
            }

            scriptingJobBean.execute(result, jobInstance);
            log.info("================finished my custom scripting job===============");

            return result;
        } catch (Exception e) {
            log.error("Exception on init/execute script", e);
            result.registerError("Error in " + scriptCode + " execution :" + e.getMessage());
        }
        return result;
    }

    @Override
    public JobCategoryEnum getJobCategory() {
        return MeveoJobCategoryEnum.MEDIATION;
    }

    @Override
    public Map<String, CustomFieldTemplate> getCustomFields() {
        Map<String, CustomFieldTemplate> result = new HashMap<>();

        CustomFieldTemplate scriptCF = new CustomFieldTemplate();
        scriptCF.setCode(MY_SCRIPTING_JOB_SCRIPT);
        scriptCF.setAppliesTo("JobInstance_MyScriptingJob");
        scriptCF.setActive(true);
        scriptCF.setDescription("Script to run");
        scriptCF.setFieldType(CustomFieldTypeEnum.ENTITY);
        scriptCF.setEntityClazz(ScriptInstance.class.getName());
        scriptCF.setValueRequired(true);
        result.put(MY_SCRIPTING_JOB_SCRIPT, scriptCF);

        return result;
    }
}