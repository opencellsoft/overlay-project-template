package com.opencell.ext.api.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.meveo.api.dto.response.SearchResponse;

import com.opencell.ext.model.Person;

/**
 * A list of Persons DTO representation
 * 
 * @author explo
 *
 */
@XmlRootElement(name = "PersonsResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class PersonsDto extends SearchResponse {

    List<PersonDto> persons;

    public PersonsDto() {
    }

    public PersonsDto(List<Person> persons2) {
        // TODO Auto-generated constructor stub
    }

    public List<PersonDto> getPersons() {
        return persons;
    }

    public void setPersons(List<PersonDto> persons) {
        this.persons = persons;
    }
}