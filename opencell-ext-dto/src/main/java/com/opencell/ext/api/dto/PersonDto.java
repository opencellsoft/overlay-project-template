package com.opencell.ext.api.dto;

import java.io.Serializable;

import com.opencell.ext.model.Person;

/**
 * Person DTO representation
 */
public class PersonDto implements Serializable {

    private static final long serialVersionUID = -1925105315531427750L;

    private Long id;

    private String name;

    private String firstname;

    private Integer age;

    public PersonDto() {
    }

    public PersonDto(Person person) {
        this.id = person.getId();
        this.name = person.getName();
        this.firstname = person.getFirstname();
        this.age = person.getAge();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
