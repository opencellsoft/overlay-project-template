package com.opencell.ext.service.script;

import java.util.Map;

import org.meveo.admin.exception.BusinessException;
import org.meveo.service.script.Script;

/**
 * Script test
 * 
 * @author mohamed stitane
 */
public class MyScriptTest extends Script {

    @Override
    public void execute(Map<String, Object> methodContext) throws BusinessException {
        log.info("EXECUTE context={}", methodContext);

    }
}