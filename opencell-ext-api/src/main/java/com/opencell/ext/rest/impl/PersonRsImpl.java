package com.opencell.ext.rest.impl;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.meveo.api.dto.ActionStatus;
import org.meveo.api.dto.ActionStatusEnum;
import org.meveo.api.dto.response.PagingAndFiltering;
import org.meveo.api.logging.WsRestApiInterceptor;
import org.meveo.api.rest.impl.BaseRs;

import com.opencell.ext.api.PersonApi;
import com.opencell.ext.api.dto.PersonsDto;
import com.opencell.ext.rest.PersonRs;

@RequestScoped
@Interceptors({ WsRestApiInterceptor.class })
public class PersonRsImpl extends BaseRs implements PersonRs {

    @Inject
    PersonApi personApi;

    @Override
    public PersonsDto list(PagingAndFiltering pagingAndFiltering) {
        try {
            return personApi.list(pagingAndFiltering);
        } catch (Exception e) {
            PersonsDto result = new PersonsDto();
            processException(e, result.getActionStatus());
            return result;
        }
    }

    public ActionStatus index() {
        ActionStatus actionStatus = new ActionStatus();
        actionStatus.setStatus(ActionStatusEnum.SUCCESS);
        actionStatus.setMessage("Test index extending api");
        return actionStatus;
    }
}
