package com.opencell.ext.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.meveo.api.rest.JacksonProvider;
import org.meveo.api.rest.JaxRsExceptionMapper;
import org.meveo.api.rest.filter.RESTCorsRequestFilter;
import org.meveo.api.rest.filter.RESTCorsResponseFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencell.ext.rest.impl.PersonRsImpl;

@ApplicationPath("/api/rest/ext")
public class ExtRestActivator extends Application {

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Set<Class<?>> getClasses() {

        Logger log = LoggerFactory.getLogger(ExtRestActivator.class);
        log.debug("Documenting {} rest services for path /api/rest/ext/");

        Set<Class<?>> resources = new HashSet();

        resources.add(RESTCorsRequestFilter.class);
        resources.add(RESTCorsResponseFilter.class);
        resources.add(JaxRsExceptionMapper.class);
        resources.add(JacksonProvider.class);

        resources.add(PersonRsImpl.class);

        return resources;
    }
}