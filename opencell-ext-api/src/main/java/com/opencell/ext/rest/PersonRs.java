package com.opencell.ext.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.meveo.api.dto.response.PagingAndFiltering;
import org.meveo.api.rest.IBaseRs;

import com.opencell.ext.api.dto.PersonsDto;

import io.swagger.v3.oas.annotations.Operation;

/**
 * @author Edward P. Legaspi
 **/
@Path("/person")
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public interface PersonRs extends IBaseRs {

    /**
     * List Person matching a given criteria
     *
     * @return List of customers
     */
    @POST
    @Path("/list")
    @Operation(summary = "List all persons", tags = { "Person management" })
    PersonsDto list(PagingAndFiltering pagingAndFiltering);
}
