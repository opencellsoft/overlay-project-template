package com.opencell.ext.api;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.api.BaseApi;
import org.meveo.api.dto.response.PagingAndFiltering;
import org.meveo.api.dto.response.PagingAndFiltering.SortOrder;

import com.opencell.ext.api.dto.PersonDto;
import com.opencell.ext.api.dto.PersonsDto;
import com.opencell.ext.model.Person;
import com.opencell.ext.service.PersonService;

@Stateless
public class PersonApi extends BaseApi {

    @Inject
    private PersonService personService;

    public PersonsDto list(PagingAndFiltering pagingAndFiltering) {

        PaginationConfiguration paginationConfig = toPaginationConfiguration("id", SortOrder.ASCENDING, null, pagingAndFiltering, Person.class);

        Long totalCount = personService.count(paginationConfig);

        PersonsDto result = new PersonsDto();

        result.setPaging(pagingAndFiltering);
        result.getPaging().setTotalNumberOfRecords(totalCount.intValue());

        if (totalCount > 0) {
            List<Person> persons = personService.list(paginationConfig);

            result.setPersons(persons.stream().map(PersonDto::new).collect(Collectors.toList()));
        }

        return result;
    }
}